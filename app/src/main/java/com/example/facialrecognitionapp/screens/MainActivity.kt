package com.example.facialrecognitionapp.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.facialrecognitionapp.R


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(
            R.id.container,
            RecognitionFragment.newInstance()
        ).commit()
    }

}
