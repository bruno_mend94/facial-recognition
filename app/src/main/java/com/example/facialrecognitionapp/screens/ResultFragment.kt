package com.example.facialrecognitionapp.screens

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.facialrecognitionapp.R
import com.example.facialrecognitionapp.toEncodedString
import kotlinx.android.synthetic.main.fragment_result.*

class ResultFragment : Fragment() {

    companion object {
        fun newInstance(faces: List<Bitmap>): ResultFragment = ResultFragment()
            .apply {
            this.faces = faces
        }
    }

    private lateinit var faces: List<Bitmap>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        image1.setImageBitmap(faces[0])
        image1.setOnClickListener{ showImageEncodedString(faces[0]) }
        image2.setImageBitmap(faces[1])
        image2.setOnClickListener{ showImageEncodedString(faces[1]) }
        image3.setImageBitmap(faces[2])
        image3.setOnClickListener{ showImageEncodedString(faces[2]) }
        image4.setImageBitmap(faces[3])
        image4.setOnClickListener{ showImageEncodedString(faces[3]) }
        image5.setImageBitmap(faces[4])
        image5.setOnClickListener{ showImageEncodedString(faces[4]) }
        image6.setImageBitmap(faces[5])
        image6.setOnClickListener{ showImageEncodedString(faces[5]) }
    }

    private fun showImageEncodedString(image: Bitmap) {
        Toast.makeText(context, image.toEncodedString(), Toast.LENGTH_LONG).show()
    }
}
