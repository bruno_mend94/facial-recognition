package com.example.facialrecognitionapp.screens

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.facialrecognitionapp.*
import com.example.facialrecognitionapp.facialrecognition.capture.FaceCapture
import com.example.facialrecognitionapp.facialrecognition.capture.FacialRecognitionStatus
import com.otaliastudios.cameraview.controls.Facing
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_recognition.*
import java.util.concurrent.TimeUnit

class RecognitionFragment : Fragment() {

    companion object {
        fun newInstance(): RecognitionFragment =
            RecognitionFragment()

        private const val REQUEST_CAMERA_PERMISSION = 123
    }

    private var disposables = CompositeDisposable()

    private lateinit var faceCapture: FaceCapture

    private val checkPermission: Boolean
        get() {
            context?.let {
                return ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            }
                ?: return false
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        faceCapture = FaceCapture(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recognition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onResume() {
        super.onResume()
        cameraView.open()
    }

    override fun onPause() {
        super.onPause()
        if (cameraView.isOpened) cameraView.close()
    }

    override fun onStop() {
        super.onStop()
        disposables.dispose()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.isNotEmpty() && !cameraView.isOpened &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                cameraView.open()
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    context?.let {
                        Toast.makeText(
                            it,
                            getString(R.string.permission_needed),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    activity?.finish()
                }
            }
        }
    }

    private fun initViews() {
        if (!checkPermission) requestPermissions(
            arrayOf(Manifest.permission.CAMERA),
            REQUEST_CAMERA_PERMISSION
        )

        with(cameraView) {
            setLifecycleOwner(this@RecognitionFragment)
            facing = Facing.FRONT
            addFrameProcessor { frame -> faceCapture.detectFace(frame) }
        }

        faceCapture.onAreaConfigured().subscribe { rectSizes ->
            maxRect.post {
                val params = maxRect.layoutParams as RelativeLayout.LayoutParams
                params.height = rectSizes.first
                params.width = rectSizes.first
                maxRect.layoutParams = params
            }
            minRect.post {
                val params = minRect.layoutParams as RelativeLayout.LayoutParams
                params.height = rectSizes.second
                params.width = rectSizes.second
                minRect.layoutParams = params
            }
        }.addTo(disposables)

        faceCapture.onFacesRecognized().subscribe {
            overlay.setImageBitmap(it)
        }.addTo(disposables)

        faceCapture.onFrameProcessed()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { setMessage(it) }
            .filter { it is FacialRecognitionStatus.Success && it.isTakingPictures }
            .throttleLast(333, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .take(6)
            .doOnNext { progressBar.progress++ }
            .map { faceCapture.getFace((it as FacialRecognitionStatus.Success).byteArray, it.rect) }
            .toList()
            .subscribe({ result ->
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(
                        R.id.container,
                        ResultFragment.newInstance(result)
                    )?.commit()
                faceCapture.dispose()
            }, {}).addTo(disposables)
    }

    private fun setMessage(status: FacialRecognitionStatus) {
        context?.let { context ->
            when (status) {
                is FacialRecognitionStatus.Error -> {
                    tvMessage.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.yellow
                        )
                    )
                    tvMessage.text = getText(R.string.face_out_of_area)
                    txtComplementaryMessage.text = status.message
                }
                is FacialRecognitionStatus.Success -> {
                    tvMessage.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.green
                        )
                    )
                    tvMessage.text = getText(R.string.blink_message)
                    txtComplementaryMessage.text = ""
                }
            }
        }
    }
}
