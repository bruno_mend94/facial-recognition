package com.example.facialrecognitionapp.facialrecognition.capture

import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.vision.face.FaceDetector
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions

internal class FaceDetector(
    cameraWidth: Int,
    cameraHeight: Int,
    private val successListener: OnSuccessListener<in List<FirebaseVisionFace>>,
    private val failureListener: OnFailureListener = OnFailureListener {}
) {

    private val detector: FirebaseVisionFaceDetector =
        FirebaseVision.getInstance()
            .getVisionFaceDetector(
                FirebaseVisionFaceDetectorOptions.Builder()
                    .setPerformanceMode(FaceDetector.ACCURATE_MODE)
                    .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                    .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                    .build()
            )

    private val metadata: FirebaseVisionImageMetadata =
        FirebaseVisionImageMetadata.Builder()
            .setWidth(cameraWidth)
            .setHeight(cameraHeight)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setRotation(FirebaseVisionImageMetadata.ROTATION_270)
            .build()

    fun detect(byteArray: ByteArray) {
        detector.detectInImage(FirebaseVisionImage.fromByteArray(byteArray, metadata))
            .addOnSuccessListener(successListener)
            .addOnFailureListener(failureListener)
    }

    fun dispose() {
        detector.close()
    }
}