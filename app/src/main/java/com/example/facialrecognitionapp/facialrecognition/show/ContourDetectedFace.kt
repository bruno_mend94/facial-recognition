package com.example.facialrecognitionapp.facialrecognition.show

import android.graphics.Rect
import com.google.firebase.ml.vision.common.FirebaseVisionPoint
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour

internal class ContourDetectedFace(
    boundingBox: Rect,
    private val faceOvalContour: FirebaseVisionFaceContour?
): DetectedFace(boundingBox) {

    fun faceContourPoints() = makeContourPoints(faceOvalContour)

    private fun makeContourPoints(vararg contours: FirebaseVisionFaceContour?): List<FloatArray> {
        val contourPoints = ArrayList<FloatArray>()
        contours.forEach {
            if (it != null) {
                contourPoints.add(makePoints(it.points as ArrayList<FirebaseVisionPoint>))
            }
        }
        return contourPoints
    }

}