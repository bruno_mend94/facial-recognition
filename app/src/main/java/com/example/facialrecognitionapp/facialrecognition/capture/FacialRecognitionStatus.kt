package com.example.facialrecognitionapp.facialrecognition.capture

import android.graphics.Rect

sealed class FacialRecognitionStatus {
    class Error(val message: String = "") : FacialRecognitionStatus()
    class Success(val byteArray: ByteArray, val rect: Rect, val isTakingPictures: Boolean) :
        FacialRecognitionStatus()
}