package com.example.facialrecognitionapp.facialrecognition.show

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour

internal class DetectionView(private val cameraWidth: Int,
                             private val cameraHeight: Int,
                             private val enableContours: Boolean = true) {

    fun showDetection(faces: List<FirebaseVisionFace>): Bitmap {
        val contourFaces = ArrayList<ContourDetectedFace>()

        faces.forEach {
            if (enableContours) {
                val contourFace =
                    ContourDetectedFace(
                        boundingBox = it.boundingBox,
                        faceOvalContour = it.getContour(FirebaseVisionFaceContour.FACE)
                    )
                contourFaces.add(contourFace)
            }
        }
        return drawDetection(contourFaces)
    }

    private fun drawDetection(contourFaces:List<ContourDetectedFace>
    ): Bitmap {
        val bmp = Bitmap.createBitmap(
            cameraHeight,
            cameraWidth,
            Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bmp)
        val paint = Paint()
        paint.strokeWidth = 4f

        contourFaces.forEach {
            with(canvas) {
                paint.color = Color.argb(180, 0, 0, 0)
                paint.style = Paint.Style.FILL
                drawRect(it.boundingBox, paint)

                paint.color = Color.RED
                paint.style = Paint.Style.STROKE
                drawRect(it.boundingBox, paint)

                paint.style = Paint.Style.STROKE
                paint.color = Color.CYAN
                it.faceContourPoints().forEach { pts -> drawLines(pts, paint) }
            }
        }

        paint.style = Paint.Style.STROKE
        return bmp
    }
}