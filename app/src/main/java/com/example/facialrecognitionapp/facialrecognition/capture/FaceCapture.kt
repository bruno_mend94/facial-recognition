package com.example.facialrecognitionapp.facialrecognition.capture

import android.content.Context
import android.content.res.Configuration
import android.graphics.*
import com.example.facialrecognitionapp.*
import com.example.facialrecognitionapp.facialrecognition.show.DetectionView
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.otaliastudios.cameraview.frame.Frame
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.SingleSubject
import kotlin.math.roundToInt

class FaceCapture(private val context: Context) {

    // As dimensões da câmera são utilizadas como parâmetro
    // para obtenção e tratamento das imagens.
    private var cameraWidth: Int = 0
    private var cameraHeight: Int = 0

    // Estes retângulos são utilizados para verificar se a face
    // está alinhada com o centro do frame.
    private lateinit var minRectSize: Rect
    private lateinit var maxRectSize: Rect

    private lateinit var faceDetector: FaceDetector
    private var currentFrame: ByteArray? = null

    // utilizado para exibir local em que a face é detectada
    private lateinit var detectionView: DetectionView

    // A obtenção de imagens é iniciada quando o usuário pisca
    // os olhos e é pausada caso o mesmo saia do centro do frmae,
    // sendo necessário piscar para voltar a capturar imagens.
    private var isTakingPictures: Boolean = false

    // Só analizamos novos frames caso a análise anterior tenha sido finalizada.
    private val isLoadingDetection: Boolean
        get() = currentFrame != null

    // Validamos se o usuário está piscando pela probabilidade de ambos
    // os olhos estarem abertos. Se a probabilidade for menor que 40%
    // assumimos que o usuário está piscando.
    private val FirebaseVisionFace.isEyesBlinking: Boolean
        get() = this.rightEyeOpenProbability in 0.0..0.4 &&
                this.leftEyeOpenProbability in 0.0..0.4

    private val onFrameProcessed: PublishSubject<FacialRecognitionStatus> = PublishSubject.create()
    fun onFrameProcessed(): Observable<FacialRecognitionStatus> = onFrameProcessed

    private val onAreaConfigured: SingleSubject<Pair<Int, Int>> = SingleSubject.create()
    fun onAreaConfigured(): Single<Pair<Int, Int>> = onAreaConfigured

    private val onFacesRecognized: PublishSubject<Bitmap> = PublishSubject.create()
    fun onFacesRecognized(): Observable<Bitmap> = onFacesRecognized

    /**
     *  Detecta se há alguma face no [frame] passado e retorna
     *  o resultado no onSuccessListener do faceDetector.
     */
    fun detectFace(frame: Frame) {
        if (!isLoadingDetection) {
            if (!::faceDetector.isInitialized) {
                cameraWidth = frame.size.width
                cameraHeight = frame.size.height
                setFaceDetector()
            }
            currentFrame = frame.getData()
            faceDetector.detect(frame.getData())
        }
    }

    /**
     * Retorna um Bitmap da face obtida em um frame com todos os tratamentos
     * necessários para enviar ao serviço de reconhecimento facial.
     *
     * @param byteArray frame com a face a ser capturada
     * @param faceRect posição da face no frame
     */
    fun getFace(byteArray: ByteArray, faceRect: Rect): Bitmap {
        val picture = byteArray.toBitmap(cameraWidth, cameraHeight)

        // Há um "bug" na lib de detação de face que faz com que ela retorne a
        // posição como se a câmera estivesse espelhada horizontalmente.
        // Para tratar isso realizamos o cálculo de espelhamento abaixo, que se baseia
        // no centro do frame para saber a posição esquerda correta da face.
        val centerHorizontal = picture.width / 2
        val faceLeftPosition = centerHorizontal + ((centerHorizontal - faceRect.left) * -1)
        // A posição superior retornada pela lib corta parte da face e cabelo.
        // Por esse motivo, pegamos a posição original e subimos ela em 50% de seu valor.
        val faceTopPosition = (faceRect.top * 0.50).roundToInt()
        // O lado direito é calculado com base no lado esquerdo com o espelhamento já corrigido.
        // Pegamos a posição do lado esquerdo corrigido e somamos o tamanho da face.
        val faceRightPosition = faceLeftPosition + (faceRect.right - faceRect.left)
        // A posição inferior retornada pela lib também corta parte da face. Por esse motivo
        // a calculamos com base na posição superior corrigida, acrescentando o tamanho da face
        // mais 50% de seu valor
        val faceBottomPosition =
            faceTopPosition + ((faceRect.bottom - faceRect.top) * 1.50).roundToInt()
        val finalFaceRect =
            Rect(faceLeftPosition, faceTopPosition, faceRightPosition, faceBottomPosition)

        return picture.cut(finalFaceRect).toGrayScale()
    }

    fun dispose() {
        if (!::faceDetector.isInitialized) faceDetector.dispose()
    }

    /**
     *  Configura o faceDetector e os parâmetros para validar
     *  se a face está posicionada no centro do frame.
     */
    private fun setFaceDetector() {
        // Para validar se a face está posicionada no centro do frame, calculamos dois retangulos.
        // O tamanho desses retângulos é obtido levando em consideração a distância do centro
        // para o lado esquerdo do frame (que aqui chamamos de apótema/apothem).
        // A apótema do retângulo maior é 75% da apótema original e a do menor 35%.
        val centerVertical = cameraWidth / 2
        val centerHorizontal = cameraHeight / 2
        val maxRectApothem = (centerHorizontal * 0.75).roundToInt()
        val minRectApothem = (centerHorizontal * 0.35).roundToInt()

        // Para obter os retângulos pegamos o centro do frame e vamos
        // somando ou subtraindo as respectivas apótemas.
        maxRectSize = Rect(
            (centerHorizontal - maxRectApothem),
            (centerVertical - maxRectApothem),
            (centerHorizontal + maxRectApothem),
            (centerVertical + maxRectApothem)
        )

        minRectSize = Rect(
            (centerHorizontal - minRectApothem),
            (centerVertical - minRectApothem),
            (centerHorizontal + minRectApothem),
            (centerVertical + minRectApothem)
        )

        onAreaConfigured.onSuccess(Pair(maxRectSize.width(), minRectSize.width()))

        faceDetector =
            FaceDetector(
                cameraWidth = cameraWidth,
                cameraHeight = cameraHeight,
                successListener = { faces ->
                    detectionView.showDetection(faces).let { onFacesRecognized.onNext(it) }
                    onFrameProcessed.onNext(faces.toFacialRecognitionStatus())
                    currentFrame = null
                })

        detectionView = DetectionView(cameraWidth, cameraHeight)
    }

    /**
     * Realiza todas as validações para obter a face:
     *  Valida se há mais de uma ou nenhuma face no frame;
     *  Valida se a face obtida está no centro do frame verificando se ela
     *  está dentro do retângulo maior e abrangendo o retângulo menor; e
     *  Valida se o dispositivo está no modo retrato.
     */
    private fun List<FirebaseVisionFace>.toFacialRecognitionStatus(): FacialRecognitionStatus {
        var errorMessage = ""
        when {
            this.isNullOrEmpty() -> errorMessage = context.getText(
                R.string.none_face
            ).toString()
            this.size > 1 -> errorMessage = context.getText(R.string.more_then_one_face).toString()
            else -> this.forEach {
                val faceBoundingBox = it.boundingBox
                val isPortraitOrientation =
                    context.resources?.configuration?.orientation == Configuration.ORIENTATION_PORTRAIT

                if (!maxRectSize.contains(faceBoundingBox))
                    errorMessage = context.getText(R.string.further_camera).toString()
                else if (!faceBoundingBox.contains(minRectSize))
                    errorMessage = context.getText(R.string.closer_camera).toString()
                else if (isPortraitOrientation) {
                    if (it.isEyesBlinking) isTakingPictures = true
                    currentFrame?.let { frame ->
                        return FacialRecognitionStatus.Success(
                            frame,
                            faceBoundingBox,
                            isTakingPictures
                        )
                    }
                }
            }
        }

        isTakingPictures = false
        return FacialRecognitionStatus.Error(errorMessage)
    }
}
