package com.example.facialrecognitionapp

import android.graphics.*
import android.util.Base64
import java.io.ByteArrayOutputStream

fun Bitmap.toEncodedString(quality: Int = 100, compressFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG): String {
    val bitmapStream = ByteArrayOutputStream()
    this.compress(compressFormat, quality, bitmapStream)
    return Base64.encodeToString(bitmapStream.toByteArray(), Base64.DEFAULT)
}

fun ByteArray.toBitmap(width: Int, height: Int): Bitmap {
    val out = ByteArrayOutputStream()
    val yuvImage = YuvImage(this, ImageFormat.NV21, width, height, null)
    yuvImage.compressToJpeg(Rect(0, 0, width, height), 30, out)
    return BitmapFactory.decodeByteArray(out.toByteArray(), 0, out.toByteArray().size).rotate()
}

fun Bitmap.rotate(degrees: Float = 270f): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(degrees)
    return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
}

fun Bitmap.cut(area: Rect): Bitmap =
    Bitmap.createBitmap(this,
        area.left,
        area.top,
        area.width(),
        area.height())

fun Bitmap.toGrayScale(): Bitmap{
    val bitmapGrayScale = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmapGrayScale)
    val color = ColorMatrix()
    color.setSaturation(0f)
    val paint = Paint()
    paint.colorFilter = ColorMatrixColorFilter(color)
    canvas.drawBitmap(this, 0f, 0f, paint)
    return bitmapGrayScale
}
